% OSU Support Vector Machines (SVMs) Toolbox
% version 2.00, Oct. 2000
%
% SVM classifier Trainer:
%
%   LinearSVC (u_LinearSVC) 
%           - construct a linear C-SVM (nu-SVM) classifier 
%               from training samples.
%   PolySVC (u_PolySVC)
%           - construct a non-linear C-SVM (nu-SVM) classifier 
%             with a polynomial kernel.
%   RbfSVC (u_RbfSVC)
%           - construct a non-linear C-SVM (nu-SVM) classifier 
%            with a radial based kernel, or Gaussian kernel.
%   one-RbfSVC
%           - construct a non-linear 1-SVM with a radial based 
%             kernel, or Gaussian kernel.
%
% C-SVC Tester:
%
%   osuSVMTest - test the performance of a trained 
%                SVM classifier
% 
% C-SVM Classifier:
%   
%   osuSVMClass - classify a set of input patterns 
%                 given a trained SVM classifier
%
% Low level functions: 
% (following functions are called by functions listed above)
% 
%   osuSVMTrain, SVMClass, SVMTest, SVMTrain,
%   mexSVMTrain, mexSVMClass,DimFit
%
% (please use 'help' to get detail information 
%   regarding these functions)
%
% Demonstration functions:
%  osusvmdemo - the main function of the command-line demonstration
%
%  lindemo (u_lindemo) 
%           -  demonstration for constructing and test a linear C-SVM, or nu-SVM, 
%              classifier
%  poldemo (u_poldemo)
%           - demonstration for constructing and test a nonlinear C-SVM, or nu-SVM, 
%              classifier with a polynomial kernel
%  rbfdemo (u_rbfdemo)
%           - demonstration for constructing and test a nonlinear C-SVM, or nu-SVM,
%              classifier with a RBF kernel
%  one_rbfdemo 
%           - demonstration for constructing and test a nonlinear 1-SVM 
%             with a RBF kernel
%  clademo (u_clademo)
%           - demonstration for classify a group of input patterns using
%             the constructed SVM classifier.
%  DemoData_train, DemoData_test, and DemoData_class - data used
%               in this demonstration. they are HRR radar signatures
%               generated from MSTAR data.
%----------------------------------------
% Author: Junshui Ma (ma.87@osu.edu)
% Information Processing System Labratory
% Department of Electrical Engineering
% the Ohio State University
%
