function [Fited]=DimFit(Data,Dim)
// USAGE:
// [Fited]=DimFit(Data,Dim)
//
// DESCRIPTION:
// Adjust the data dimension to Dim.
// * add and append zeros if data is shorter
// * trim if data is longer
//
// INPUTS:
// Data: a row of column vectors (Matrix)
// Dim: the desired dimenstion (Scalar)
//
// OUTPUT:
// Fited: the data after adjusting the dimension
//

[N l]=size(Data);
if N==Dim then
  Fited = Data;
  return;
end

if N>Dim then
  halfdiff = floor((N-Dim)/2);
  Fited = Data(halfdiff+1:Dim+halfdiff,:);
else
  halfdiff = ceil((Dim-N)/2);
  Fited = [zeros(halfdiff,l); Data; zeros(Dim-N-halfdiff,l)];
end
endfunction
