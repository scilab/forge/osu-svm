path = get_absolute_file_path('test_svm.sce');

loadmatfile(path + 'data/heart_scale.mat');

/////////////////////////////
// Test with a full matrix //
/////////////////////////////

libsvmwrite('test_full.dat',heart_scale_label,heart_scale_inst);

[heart_scale_label_full,heart_scale_inst_full] = libsvmread('test_full.dat');

if (and(heart_scale_label==full(heart_scale_label_full))) then 
  printf('full matrix -> label are equals\n');
end
if (and(heart_scale_inst==full(heart_scale_inst_full))) then 
  printf('full matrix -> inst are equals\n');
end

///////////////////////////////
// Test with a sparse matrix //
///////////////////////////////

libsvmwrite('test_sparse.dat',heart_scale_label,sparse(heart_scale_inst));
[heart_scale_label_sparse,heart_scale_inst_sparse] = libsvmread('test_sparse.dat');

if (and(heart_scale_label==heart_scale_label_sparse)) then 
  printf('sparse matrix -> label are equals\n');
end
if (and(heart_scale_inst==heart_scale_inst_sparse)) then 
  printf('sparse matrix -> inst are equals\n');
end
