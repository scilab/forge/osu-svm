path = get_absolute_file_path('u_poldemo.sce');

mode(-1);

//POLDEMO demonstration for using nonlinear SVM classifier with a 
// polynomial keneral.

mode(1);
clc();

//POLDEMO demonstration for using nonlinear SVM classifier with a 
// polynomial keneral.
//##########################################################################
//
//   This is a demonstration script-file for contructing and 
//     testing a nonlinear SVM-based classifier 
//     (with a polynomial kernel) using OSU SVM CLASSIFIER TOOLBOX. 
//   Note that the form of the polynomial kernel is 
//                (Gamma*<X(:,i),X(:,j)>+Coefficient)^Degree
//
//##########################################################################

pause; // Strike any key to continue (Note: use Ctrl-C to abort)
clc();

//##########################################################################
//
//   Load the training data and examine the dimensionity of the data
//
//##########################################################################

pause; // Strike any key to continue 

// load the training data

loadmatfile(path + 'data/DemoData_train.mat');

pause; // Strike any key to continue 

// take a look at the data, and please pay attention to the dimensions 
// of the input data 

who

size(Labels) 
size(Samples)

pause; // Strike any key to continue 
clc();

//##########################################################################
//
//   Construct a nonlinear SVM classifier (with polynomial kernel) 
//     using the training data
//   Note that the form of the polynomial kernel is 
//      (Gamma*<X(:,i),X(:,j)>+Coefficient)^Degree
//
//##########################################################################

pause; // Strike any key to continue 

// set the value of Degree if you don't want use its default value, 
// which is 3.

Degree = 5;

// By using this format, the default values of Gamma, Coefficient,
// u, Epsilon, CacheSize are used. 
// That is, Gamma=1, Coefficient=1, u=0.5, Epsilon=0.001, and CacheSize=35MB

[AlphaY, SVs, Bias, Parameters, Ns] = u_PolySVC(Samples, Labels, Degree);

// End of the SVM classifier construction 
//
// The resultant SVM classifier is jointly determined by 
//  "AlphaY", "SVs", "Bias", "Parameters", and "Ns".
//

pause; // Strike any key to continue 

// Save the constructed nonlinear SVM classifier 

savematfile(path + 'data/SVMClassifier.mat','Ns','AlphaY','SVs','Bias','Parameters');

pause; // Strike any key to continue 
clc();

//##########################################################################
//
//   Test the constructed nonlinear SVM Classifier
//
//##########################################################################

pause; // Strike any key to continue 

// Load the constructed nonlinear SVM classifier

loadmatfile(path + 'data/SVMClassifier.mat');

pause; // Strike any key to continue 

// have a look at the variables determining the SVM classifier

who

pause; // Strike any key to continue 

// load test data

loadmatfile(path + 'data/DemoData_test.mat');

pause; // Strike any key to continue 

// Test the constructed SVM classifier using the test data
// begin testing ...

[ConfMatrix, scores] = osuSVMTest(Samples, Labels, Ns, AlphaY, SVs, Bias, Parameters);

// end of the testing

pause; // Strike any key to continue 

// The resultant confusion matrix of this 4-class classification problem is:

ConfMatrix

pause; // Strike any key to continue 
mode(-1);
