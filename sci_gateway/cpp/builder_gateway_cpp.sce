// This file is released into the public domain

svm_path = get_absolute_file_path("builder_gateway_cpp.sce");

Table = ["svm_class",   "sci_svm_class"; ...
         "svm_train",   "sci_svm_train"; ...
         "libsvmread",  "sci_libsvmread"; ...
         "libsvmwrite", "sci_libsvmwrite"];

Files = ["sci_svm_class.o","sci_svm_train.o","mzSVMCommon.o","svm.o","external_data.o", ...
         "libsvmread.o","libsvmwrite.o"];

CFLAGS = '-ggdb -I' + svm_path;

tbx_build_gateway("svm_cpp", Table, Files, svm_path, "", "", CFLAGS);

clear tbx_build_gateway;
