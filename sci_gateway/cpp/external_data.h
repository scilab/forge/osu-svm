#ifndef EXTERNAL_DATA_H
#define EXTERNAL_DATA_H

extern double svmInfo_epsilon_nu;
extern double svmInfo_nu;
extern double svmInfo_C;
extern double svmInfo_object;
extern int svmInfo_iter_num;
#endif
