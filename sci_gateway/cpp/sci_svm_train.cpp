/*  sci_svm_train.cpp

    Scilab C++ code for SVM training.

    This provides a Scilab interface of LIBSVM 2.33.

    Refer to http://www.csie.ntu.edu.tw/~cjlin for more information
      on LIBSVM.

    By Yi Zhao, OSU-EE, 01/20/2002 (version 2.00).
    By Yi Zhao, OSU-EE, 12/31/2001 (version 1.10).
    By Yi Zhao, OSU-EE, 12/28/2001 (version 1.00).

    Modifiedy by Junshui Ma, 02/12/2002 to fit in the 
	the OSU SVM framework.

    Scilab version by Y. Collette
*/

#include <stdio.h>

extern "C" {
#include <api_scilab.h>
#include <stack-c.h>
#include <sciprint.h>
#include <MALLOC.h>
#include <Scierror.h>
}

// LIBSVM
#include "svm.h"

// mzSVM common module
#include "mzSVMCommon.h"
#include "external_data.h"

// Gateway function to Scilab
//
//Scilab Function:
// [AlphaY, SVs, Bias, Parameters] = mexSVMTrain(Samples, Labels)
// [AlphaY, SVs, Bias, Parameters, nSV, nLabel] = mexSVMTrain(Samples, Labels, Parameters)
// [AlphaY, SVs, Bias, Parameters, nSV, nLabel] = mexSVMTrain(Samples, Labels, Parameters, Weight)
// [AlphaY, SVs, Bias, Parameters, nSV, nLabel] = mexSVMTrain(Samples, Labels, Parameters, Weight, Verbose)
//
// Inputs:
//    Samples    - training patterns, MxN;
//    Labels     - labels of training patterns, 1xN;
//    Parameters - input parameters;
//    Weight     - weighting factors of patterns 1xA, where A<=L;
//    Verbose    - verbose level
//                0 : very silent (default);
//                1 : a little verbose;
//                2 : very verbose.
// Outputs:
//    AlphaY    - coef. of SVs, (L-1) x sum(nSV);
//    SVs       - SVs, M x sum(nSV);
//    Bias      - bias of classifier(s), 1 x L*(L-1)/2;
//    Parameters -  parameters used in training.
//    nSV       -  numbers of SV in each class, 1xL;
//    nLabel    -  Labels of each class, 1xL;

extern "C" int sci_svm_train(char * fname)
{
  svm_problem prob;
  svm_parameter parame;
  svm_model * model;
  svm_node * x_space;

  int * verbose_addr = NULL, * X_addr = NULL, * Y_addr = NULL, * Par_addr = NULL, * WT_addr = NULL;
  int nX, mX, nY, mY, nPar, mPar, nWT, mWT;
  double dbl_verbose, * pdbl_X = NULL, * pdbl_Y = NULL, * pdbl_Par = NULL, * pdbl_WT = NULL;

  double * pdbl_dLabels = NULL, * pdbl_dnSVs = NULL, * pdbl_SVs = NULL, * pdbl_alphaY = NULL, * pdbl_Bias = NULL, * pdbl_outPar = NULL;
  int nClass, nSV, nBias;
  // Verbose level.
  int mzVerbose;
  SciErr _SciErr;
  
  // Test the input arguments.
  if (Rhs < 2 || Rhs > 5)
    {
      Scierror(999,"%s: Wrong number of input arguments!\n",fname);
      return 0;
    }

  if (Rhs < 5)
    {
      mzVerbose = 0;
    }
  else 
    {
      _SciErr = getVarAddressFromPosition(pvApiCtx, 5, &verbose_addr); SCISVM_ERROR;
      getScalarDouble(pvApiCtx, verbose_addr, &dbl_verbose);
      mzVerbose = (int) dbl_verbose;
    }

  if (mzVerbose>0) printf("%s:\n",fname);

  // Read problem.
  if (mzVerbose>0) printf("1. Read problem ... ");

  // patterns
  _SciErr = getVarAddressFromPosition(pvApiCtx, 1, &X_addr); SCISVM_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, X_addr, &mX, &nX, &pdbl_X); SCISVM_ERROR;
  // labels
  _SciErr = getVarAddressFromPosition(pvApiCtx, 2, &Y_addr); SCISVM_ERROR;
  _SciErr = getMatrixOfDouble(pvApiCtx, Y_addr, &mY, &nY, &pdbl_Y); SCISVM_ERROR;
  if (nX!=nY)
    {
      Scierror(999, "%s: Sizes of samples and labels do not match. \n",fname);
      return 0;
    }

  x_space = mzSVMReadProblem(&prob, mX, nX, pdbl_X, pdbl_Y);

  if (mzVerbose>0) printf("OK!\n");

  // Read input paremeters.
  if (mzVerbose>0) printf("2. Read parameters ... ");

  mzSVMInitParameter(&parame);

  if (Rhs >= 3) 
    {
      _SciErr = getVarAddressFromPosition(pvApiCtx, 3, &Par_addr); SCISVM_ERROR;
      _SciErr = getMatrixOfDouble(pvApiCtx, Par_addr, &mPar, &nPar, &pdbl_Par); SCISVM_ERROR;

      if (mPar!=1)
        {
          Scierror(999,"%s: parameters is not a row vector!",fname);
          return 0;
        }

      mzSVMReadParameter(&parame, pdbl_Par, nPar);
    }

  if (parame.gamma == 0) parame.gamma = 1.0/mX;

  if (mzVerbose>0) printf("OK!\n");
	
  // Read weighting coefficients if available.
  if (Rhs >= 4) 
    {
      _SciErr = getVarAddressFromPosition(pvApiCtx, 4, &WT_addr); SCISVM_ERROR;
      if (!isEmptyMatrix(pvApiCtx, WT_addr))
        {
          _SciErr = getMatrixOfDouble(pvApiCtx, WT_addr, &mWT, &nWT, &pdbl_WT); SCISVM_ERROR;
          if ((mWT!=1)&&(nWT!=1))
            {
              Scierror(999,"%s: WT is not vector!",fname);
              return 0;
            }
          if (nWT==1) nWT = mWT;

          mzSVMReadWeight(&parame, pdbl_WT, nWT);
        }
    }

  // Train SVM classifer(s).
  if (mzVerbose>0) printf("3. Train SVM model ... ");

  model = svm_train(&prob, &parame);

  if (mzVerbose>0) printf("OK!\n");

  // Create output arguments.
  nClass = model->nr_class;
  nSV    = model->l;
  nBias  = nClass * (nClass-1) / 2;

  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+1, nClass-1, nSV, &pdbl_alphaY); SCISVM_ERROR; // SV coef. or AlphaY.
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+2, mX, nSV, &pdbl_SVs); SCISVM_ERROR;
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+3, 1, nBias, &pdbl_Bias); SCISVM_ERROR;
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+4, 1, 10, &pdbl_outPar); SCISVM_ERROR;
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+5, 1, nClass, &pdbl_dnSVs); SCISVM_ERROR;
  _SciErr = allocMatrixOfDouble(pvApiCtx, Rhs+6, 1, nClass, &pdbl_dLabels); SCISVM_ERROR;

  // Save output arguments.
  if (mzVerbose>0) printf("4. Output SVM model ... ");

  sciprint("DEBUG: nClass = %d class = %d\n", nClass, model->nr_class);

  // Save model to output arguments.
  mzSVMSaveModel(model, pdbl_dLabels, pdbl_dnSVs, pdbl_SVs, pdbl_alphaY, pdbl_Bias, mX);

  // Save output parameters.
  mzSVMSaveParameter(&(model->param), pdbl_outPar);

  if (mzVerbose>0) printf("OK!\n");

  // Free memory used in model.
  if (mzVerbose>0) printf("5. Clear memory ... ");

  LhsVar(1) = Rhs+1;
  if (Lhs>=2) LhsVar(2) = Rhs+2;
  if (Lhs>=3) LhsVar(3) = Rhs+3;
  if (Lhs>=4) LhsVar(4) = Rhs+4;
  if (Lhs>=5) LhsVar(5) = Rhs+5;
  if (Lhs>=6) LhsVar(6) = Rhs+6;

  svm_free_and_destroy_model(&model);

  if (mzVerbose>0) printf("OK!\n");

  // Free temporary memory.
  if (prob.y)  {FREE(prob.y);  prob.y  = NULL;}
  if (prob.x)  {FREE(prob.x);  prob.x  = NULL;}
  if (x_space) {FREE(x_space); x_space = NULL;}

  return 0;
}
