/*  mzSVMCommon.cpp

    SVM common module, which provides common functions used in Scilab
      interface of LIBSVM 2.33.

    By Yi Zhao, OSU-EE, 01/20/2002 (version 2.00).
    By Yi Zhao, OSU-EE, 12/31/2001 (version 1.10).
    By Yi Zhao, OSU-EE, 12/22/2001 (version 1.00).

    Modifiedy by Junshui Ma, 02/12/2002 to fit in the 
	the OSU SVM framework.

    Scilab version by Y. Collette
*/

#ifndef __mzSVMCommon_CPP_
#define __mzSVMCommon_CPP_

extern "C" {
#include <MALLOC.h>
}

#include "svm.h"
#include "mzSVMCommon.h"


/*  mzFindIndex : Find index of key in an array. */
int mzSVMFindIndex(const double x[], double key, int size)
{
  int index;

  for(index=0; index<size; index++) if(x[index]==key) return(index);
  return(-1);
}

/*  mzSVMInitParameter : Initialize kernel parameters. */
void mzSVMInitParameter(svm_parameter * Parame)
{
  // Initialize parameters.
  Parame->kernel_type  = 2;			// RBF
  Parame->degree       = 3;
  Parame->gamma        = 1;
  Parame->coef0        = 0;
  Parame->C            = 1;
  Parame->cache_size   = 40;
  Parame->eps          = 0.001;
  Parame->svm_type     = 0;			// C-SVM
  Parame->nu           = 0.5;
  Parame->p            = 0.1;
  Parame->shrinking    = 1;
  Parame->nr_weight    = 0;
  Parame->weight_label = NULL;
  Parame->weight       = NULL;
}

/*  mzSVMReadParameter : Read kernel parameters. */
void mzSVMReadParameter(svm_parameter * Parame, const double Paras[], int nPar)
{
  // Read parameters.
  if(nPar > 0)  Parame->kernel_type  = (int) Paras[0];
  if(nPar > 1)  Parame->degree       = Paras[1];
  if(nPar > 2)  Parame->gamma        = Paras[2];
  if(nPar > 3)  Parame->coef0        = Paras[3];
  if(nPar > 4)  Parame->C            = Paras[4];
  if(nPar > 5)  Parame->cache_size   = Paras[5];
  if(nPar > 6)  Parame->eps          = Paras[6];
  if(nPar > 7)  Parame->svm_type     = (int) Paras[7];
  if(nPar > 8)  Parame->nu           = Paras[8];
  if(nPar > 9)  Parame->p            = Paras[9];
  if(nPar > 10) Parame->shrinking    = (int) Paras[10];
}

/* mzSVMSaveParameter : Save kernel parameters. */
void mzSVMSaveParameter(const svm_parameter * Parame, double Paras[])
{
  // Save parameters.
  Paras[0] = (double) Parame->kernel_type;
  Paras[1] = (double) Parame->degree;
  Paras[2] = (double) Parame->gamma;
  Paras[3] = (double) Parame->coef0;
  Paras[4] = (double) Parame->C;
  Paras[5] = (double) Parame->cache_size;
  Paras[6] = (double) Parame->eps;
  Paras[7] = (double) Parame->svm_type;
  Paras[8] = (double) Parame->nu;
  Paras[9] = (double) Parame->p;
}

/*  mzSVMReadWeight : Read weighting coefficients. */
void mzSVMReadWeight(svm_parameter * Parame, const double Weight[], int nWT)
{
  int i;
  // Read weighting coefficients.
  if (nWT!=0) 
    {
      Parame->nr_weight    = nWT;
      Parame->weight_label = (int *)MALLOC(nWT*sizeof(int));
      Parame->weight       = (double *)MALLOC(nWT*sizeof(double));
      for(i=0; i<nWT; i++)
        {
          Parame->weight_label[i] = i;
          Parame->weight      [i] = Weight[i];
        }
    }
}

/*  mzSVMReadProblem : Fill svm_problem with data from X & Y. */
svm_node * mzSVMReadProblem(svm_problem * prob, int mX, int nX, const double X[], const double Y[])
{
  svm_node * x_space;
  int elements, i, j, k, offset;

  // Set number of patterns.
  prob->l = nX;

  // Count non-zero elements in X.
  for (elements=0, offset=0, j=0; j<nX; j++)
    for (i=0; i<mX; i++)
      if (X[offset++])  elements ++;

  // Allocate memory to store problem.
  prob->y = (double *)MALLOC(prob->l*sizeof(double));
  prob->x = (svm_node **)MALLOC(prob->l*sizeof(svm_node *));
  x_space = (svm_node *)MALLOC((elements+prob->l)*sizeof(svm_node));

  // Fill prob with X & Y.
  for (offset=0, k=0, j=0; j<nX; j++) 
    {
      prob->x[j] = &x_space[k];
      prob->y[j] = Y[j];
      for (i=0; i<mX; i++) 
        {
          if (X[offset] != 0) 
            {
              x_space[k].index = i;
              x_space[k].value = X[offset];
              k ++;
            }
          offset ++;
        }
      x_space[k++].index = -1;
    }
  return(x_space);
}

/* mzSVMReadModel : Read model. */
void mzSVMReadModel(svm_model * model, const double Labels[],
                    const double nSVs[], const double SVs[], const double alphaY[],
                    const double Bias[], int nClass, int mX)
{
  svm_node * x_space;
  int nSV, nBias, elements, i, j, k, offset;

  // Read Labels.
  model->nr_class = nClass;
  model->label    = (int *)MALLOC(nClass*sizeof(int));
  for(i=0; i<nClass; i++)
    model->label[i] = (int) Labels[i];

  // Read nSVs.
  model->nSV = (int *)MALLOC(nClass*sizeof(int));
  for(nSV=0, i=0; i<nClass; i++) 
    {
      model->nSV[i] = (int) nSVs[i];
      nSV += (int) nSVs[i];
    }

  // Read SVs.
  model->l  = nSV;
  model->SV = (svm_node **)MALLOC(nSV*sizeof(svm_node *));

  // Count non-zero elements in SVs.
  for(elements=0, offset=0, j=0; j<nSV; j++)
    for(i=0; i<mX; i++)
      if (SVs[offset++]!=0)  elements ++;

  x_space = (svm_node *)MALLOC((nSV+elements)*sizeof(svm_node));

  for(k=0, offset=0, j=0; j<nSV; j++) 
    {
      model->SV[j] = &x_space[k];
      for(i=0; i<mX; i++) 
        {
          if (SVs[offset]!=0) 
            {
              x_space[k].index = i;
              x_space[k].value = SVs[offset];
              k ++;
            }
          offset ++;
        }
      x_space[k++].index = -1;
    }

  // Read SV coef.
  model->sv_coef = (double **)MALLOC((nClass-1)*sizeof(double *));
  for(offset=0, i=0; i<nClass-1; i++)
    model->sv_coef[i] = (double *)MALLOC(nSV*sizeof(double));

  for(offset=0, j=0; j<nSV; j++)
    for(i=0; i<nClass-1; i++)
      model->sv_coef[i][j] = alphaY[offset++];

  // Read Bias.
  nBias = nClass * (nClass-1) / 2;
  model->rho = (double *)MALLOC(nBias*sizeof(double));
  for(j=0; j<nBias; j++)
    model->rho[j] = Bias[j];

  // Set XXX.
  model->free_sv = 0;
}

/* mzSVMSaveModel : Save model. */
void mzSVMSaveModel(const svm_model * model, double Labels[],
                    double nSVs[], double SVs [], double alphaY[], double Bias[],
                    int mX)
{
  svm_node * np;
  int nClass, nSV, nBias, i, j, offset;

  // Save Labels.
  nClass = model->nr_class;
  for(i=0; i<nClass; i++)
    if (model->label == NULL)
      Labels[i] = 0.0;
    else
      Labels[i] = (double) model->label[i];

  // Save nSVs.
  for(i=0; i<nClass; i++)
    if (model->nSV == NULL)
      nSVs[i] = 0;
    else
      nSVs[i] = (double) model->nSV[i];

  // Save SVs.
  nSV = model->l;
  for(offset=0, j=0; j<nSV; j++)
    {
      np = model->SV[j];
      for(i=0; i<mX; i++)
        SVs[offset+i] = 0;
      while(np->index!=-1) 
        {
          SVs[offset+np->index] = np->value;
          np ++;
        }
      offset += mX;
    }

  // Save SV coef.
  for(offset=0, j=0; j<nSV; j++)
    for(i=0; i<nClass-1; i++)
      alphaY[offset++] = model->sv_coef[i][j];

  // Save Bias.
  nBias  = nClass * (nClass-1) / 2;
  for(j=0; j<nBias; j++)
    Bias[j] = model->rho[j];
}

void mzSVMDestroyModel(svm_model* model)
{
  int i;

  FREE(model->label);
  FREE(model->nSV);
  FREE(model->SV);
  for(i=0;i<model->nr_class-1;i++)
    FREE(model->sv_coef[i]);
  FREE(model->sv_coef);
  FREE(model->rho);
}

#endif
